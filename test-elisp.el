#!/usr/bin/env elisp

(setq name (read-input "What is your name? "))

(print-format "Hello, %s!\n" name)
